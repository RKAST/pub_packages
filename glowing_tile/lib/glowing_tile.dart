library glowing_tile;

import 'package:flutter/material.dart';

class GlowingTile extends StatefulWidget {
  final Widget widget;
  final Duration duration;

  const GlowingTile({@required this.widget,this.duration=const Duration(milliseconds: 400)});
  @override
  _GlowingTileState createState() => _GlowingTileState();
}

class _GlowingTileState extends State<GlowingTile>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void initState() {
    _animationController = new AnimationController(
        vsync: this, duration: widget.duration);
    _animationController.repeat(reverse: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: _animationController,
      child: widget.widget,
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
