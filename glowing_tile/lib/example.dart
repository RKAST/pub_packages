import 'package:flutter/material.dart';
import 'package:glowing_tile/glowing_tile.dart';

class Screen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: GlowingTile(
          widget: Container(
            width: 300,
            height: 300,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(300),
              color: Colors.red,
            ),
            child: Center(
              child: Text('you can add anything',style: TextStyle(color: Colors.white),),
            ),
          ),
          duration: Duration(milliseconds: 400),
        ),
      ),
    );
  }
}