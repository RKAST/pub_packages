# glowing_tile

Glowing Tile is a package for both android and iOSwhich glows continuously.This can be helpful in bringing users attention on your widget.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

## Screenshots

<img src="ss1.png" height="300em" /> <img src="ss2.png" height="300em" /> <img src="ss3.png" height="300em" /> 


## To use this package :

* add the dependency to your pubspec.yaml

```yaml
    dependencies:
      flutter:
        sdk: flutter
      
      glowing_tile: ^0.0.1
```

## How to use


```dart
class Screen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: GlowingTile(
          widget: Container(
            width: 300,
            height: 300,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(300),
              color: Colors.red,
            ),
            child: Center(
              child: Text('you can add anything',style: TextStyle(color: Colors.white),),
            ),
          ),
          duration: Duration(milliseconds: 400),
        ),
      ),
    );
  }
}
```
# License
    
    Copyright 2020 Rajkiran
   